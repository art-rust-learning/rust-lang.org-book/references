fn main() {

    { // use a reference as a parameter
        let s1 = String::from("hello");
        let len = calculate_length(&s1);

        println!("The length of '{}' is {}.", s1, len);

        fn calculate_length(s: &String) -> usize {
            s.len()
        }
    };

    { // trying to modify a string passed as reference
        //let s = String::from("hello");
        let mut s = String::from("hello");

        //change(&s);
        change(&mut s);

        //fn change(some_string: &String) {
        fn change(some_string: &mut String) {
            some_string.push_str(", world");
        }
    };

    { // Only ONE mutable reference in a scope is allowed
        let mut s = String::from("hello");

        let r1 = &mut s;
        //let r2 = &mut s;

        // println!("{}, {}", r1, r2);
        println!("{}", r1);
    };

    { // Cannot borrow as mutable if borrowed as immutable
        // let mut s = String::from("hello");
        let s = String::from("hello");


        let r1 = &s; // no problem
        let r2 = &s; // no problem
        // let r3 = &mut s; // BIG PROBLEM

        // println!("{}, {}, and {}", r1, r2, r3);
        println!("{}, {}", r1, r2);

    };

    { // Mutable borrow after immutable gone out of scope if fine
        let mut s = String::from("hello");

        let r1 = &s; // no problem
        let r2 = &s; // no problem
        println!("{}, {}", r1, r2);

        let r3 = &mut s;

        println!("{}", r3);
    };

    { // Dangling references
        let reference_to_nothing = dangle();

        // fn dangle() -> &String {
        fn dangle() -> String {

            let s = String::from("hello");

            s
        }

        println!("{}", reference_to_nothing);
    };

}
